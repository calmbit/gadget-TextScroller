gadget-TextScroller
===================
A direct fork of the [Text Scroller](https://github.com/Rise-Vision/gadgets/tree/master/TextScroller) gadget, albeit with a minor, rough change so it will poll from an API endpoint rather than displaying static text.